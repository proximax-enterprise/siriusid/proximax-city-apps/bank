import { Component, OnInit } from '@angular/core';
import { ChangeDataService } from 'src/services/change-data.service';
import { Listener, NetworkType } from 'tsjs-xpx-chain-sdk';
import { LoginRequestMessage, VerifytoLogin, CredentialRequestMessage, Credentials ,ApiNode, CredentialRequired} from 'siriusid-sdk';
import { verifyHostBindings } from '@angular/compiler';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ClientInfoService } from 'src/services/client-info.service';
import { LoginVerificationService } from '../services/login-verification/login-verification.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {

  currentNode = "api-1.testnet2.xpxsirius.io";
  subscribe;
  listener: Listener;
  connect = false;
  qrImg;
  showImg = false;
  qrCredential;
  alert = true;
  type;
  atmCard;
  quantity;
  issue;
  feePayment;
  holderName;
  internetBanking;
  smsAlert;
  aggree = false;
  flag = true;
  
  linkLogin;
  linkCredential;

  warning = false;
  warning2 = false;
  step0 = false;
  step1 = true;
  step2 = false;
  step3 = false;
  step4 = false;
  step5 = false;
  step6 = false;
  messErr;
  // nationalID = false;
  // employmentID = false;
  credentialChosen = ["proximaxId","passport"];
  credential1;
  credential2;
  constructor(
    private changeDataService: ChangeDataService,
    private router:Router,
    public alertController:AlertController,
    private clientInfo:ClientInfoService,
    private loginVerificationService: LoginVerificationService
  ) { 
    console.log('api node: ' + this.currentNode);
    ApiNode.apiNode = "https://"+this.currentNode;
    ApiNode.networkType = NetworkType.TEST_NET;//TEST_NET; 
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    if(this.flag){
      this.step1 = false;
      this.step0 = true;
      this.flag = false;
    } else {
      this.start();
    }
  }

  ngOnInit() {
  }

  async loginRequestAndVerify(){
    this.connect = false;
    this.credential1 = CredentialRequired.create(this.credentialChosen[0]);
    this.credential2 = CredentialRequired.create(this.credentialChosen[1]);
    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp,[this.credential1,this.credential2]);
    const sessionToken = loginRequestMessage.getSessionToken();
    this.qrImg = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 &&  !this.connect){
        this.warning2 = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection(sessionToken);
        
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg = true;
      }
      else if(this.connect){
        clearInterval(interval);
      }
    }, 1000);
  }
  
  async connection(sessionToken) {
    
    this.listener.open().then(() => {
      this.warning2 = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        
        let verify = this.loginVerificationService.verify(transaction, sessionToken,[this.credential1,this.credential2],this.changeDataService.privateKeydApp);

        if (verify){
          this.connect = true;
          console.log("Transaction matched");
          this.changeDataService.addressSiriusid = transaction.signer.address.plain();
          this.subscribe.unsubscribe();
          this.step3 = false;          
          this.step4 = true;
        }
        else console.log("Transaction not match");  
             
      });
    })
    
  }

  refresh(){
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }
  async continue(){
    if (this.type && this.atmCard && this.internetBanking && this.smsAlert && this.issue && this.feePayment && this.holderName && this.aggree && this.quantity <= 10 && this.quantity >=0){
        let content = new Map<string,string>([
          ["Type of Account", this.type],
          ["ATM Card", this.atmCard],
          ['Quantity of Card', this.quantity],
          ['Issue Type', this.issue],
          ['Fee Payment', this.feePayment],
          ['Primary Card Holder name embosses (in capital letter)', this.holderName.toUpperCase()],
          ['Internet Banking Service', this.internetBanking],
          ['SMS Alerts Service', this.smsAlert],
        ])

        const credential = Credentials.create(
          'bankID',
          'Banking System',
          'Banking IDentification',
          '/assets/bank.svg',
          [],
          content,
          "",
          Credentials.authCreate(content,this.changeDataService.privateKeydApp)

        );
        const msg = CredentialRequestMessage.create(credential);

        this.qrCredential = await msg.generateQR();
        this.linkCredential = await msg.universalLink();

        this.warning = false;
        this.step4 = false;
        this.step5 = true;
      }
    else if (this.quantity > 10 || this.quantity < 0){ 
      this.messErr = "*Maximum quantity is 10 and minimum is 0";
      this.warning = true;
    }
    else{
      this.messErr = "*Fill out the form above to create an account";
      this.warning = true;
    }
  }

  start(){
    this.step0 = false;
    this.step1 = false;
    this.step2 = true;
  }


  
  login(){
    this.step2 = false;
    this.step3 = true;
    this.loginRequestAndVerify();
    
  }
  finish(){
    this.step5 = false;
    this.step6 = true;
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.connect = true;
    // this.subscribe.unsubscribe();
    this.step1= true;
    this.step0 = false;
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    this.step5 = false;
    this.step6 = false;

    this.router.navigate(['/check-result']);

  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }
}
