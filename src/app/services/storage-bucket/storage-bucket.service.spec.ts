import { TestBed } from '@angular/core/testing';

import { StorageBucketService } from './storage-bucket.service';

describe('StorageBucketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorageBucketService = TestBed.get(StorageBucketService);
    expect(service).toBeTruthy();
  });
});
