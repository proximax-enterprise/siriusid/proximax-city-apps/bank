import { Component, OnInit } from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.page.html',
  styleUrls: ['./show-result.page.scss'],
})
export class ShowResultPage implements OnInit {

  type;
  atmCard;
  quantity;
  issue;
  feePayment;
  holderName;
  internetBanking;
  smsAlert;
  connect = false;
  subscribe;

  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
  ) {
    let content = this.clientInfo.clientInfo[0].content;
    this.type = content[0][1];
    this.atmCard = content[1][1];
    this.quantity = content[2][1];
    this.issue = content[3][1];
    this.feePayment = content[4][1];
    this.holderName = content[5][1];
    this.internetBanking = content[6][1];
    this.smsAlert = content[7][1];


  }

  ngOnInit() {
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/application']);
  }

}
