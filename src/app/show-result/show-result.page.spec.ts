import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowResultPage } from './show-result.page';

describe('ShowResultPage', () => {
  let component: ShowResultPage;
  let fixture: ComponentFixture<ShowResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowResultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
