import { TestBed } from '@angular/core/testing';

import { ChangeDataService } from './change-data.service';

describe('ChangeDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChangeDataService = TestBed.get(ChangeDataService);
    expect(service).toBeTruthy();
  });
});
